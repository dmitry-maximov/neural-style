FROM ubuntu:14.04

RUN apt-get update && \
    apt-get install -y curl git libprotobuf-dev protobuf-compiler \
    software-properties-common python-software-properties luarocks

RUN curl -s https://raw.githubusercontent.com/torch/ezinstall/master/install-deps | bash
RUN git clone https://github.com/torch/distro.git /torch --recursive
RUN cd /torch; ./install.sh

RUN echo '#!/bin/bash\n\
source /torch/install/bin/torch-activate\n\
luarocks install loadcaffe\n'\
>> /tmp/0luarocks.sh && \
   chmod +x /tmp/0luarocks.sh && /tmp/0luarocks.sh rm /tmp/0luarocks.sh

RUN git clone https://github.com/jcjohnson/neural-style.git
WORKDIR /neural-style
RUN sh models/download_models.sh

COPY entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

# CUDA
# RUN wget http://developer.download.nvidia.com/compute/cuda/7_0/Prod/local_installers/rpmdeb/cuda-repo-ubuntu1404-7-0-local_7.0-28_amd64.deb && \
#  dpkg -i cuda-repo-ubuntu1404-7-0-local_7.0-28_amd64.deb && rm cuda-repo-ubuntu1404-7-0-local_7.0-28_amd64.deb
# #
# RUN apt-get update && apt-get install -y cuda
#
# RUN wget http://developer.download.nvidia.com/compute/redist/cudnn/v4/cudnn-7.0-linux-x64-v4.0-prod.tgz && \
#      tar -xzvf cudnn-7.0-linux-x64-v4.0-prod.tgz && \
#      sudo cp cuda/lib64/libcudnn* /usr/local/cuda-7.0/lib64/ && \
#      sudo cp cuda/include/cudnn.h /usr/local/cuda-7.0/include/ && rm cudnn-7.0-linux-x64-v4.0-prod.tgz
# #
# RUN echo $'#!/bin/bash\n\
# source /torch/install/bin/torch-activate\n\
# luarocks install cutorch\n\
# luarocks install cunn\n\
# luarocks install cudnn\n'\
# >> /tmp/0luarocks.sh && \
#    chmod +x /tmp/0luarocks.sh && /tmp/0luarocks.sh && rm /tmp/0luarocks.sh
